//
//  ViewController.swift
//  Exemple audio
//
//  Created by Francesc Roig i Feliu on 21/11/16.
//

import UIKit
import AVFoundation



class ViewController: UIViewController {

    // Componente reproductor de audio.
    @objc var player: AVAudioPlayer!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        cargaAudio(nombre: "song", player: &player)
    }


    
    // Acción boton "Reproduce canción 1".
    @IBAction func accionBotonReproduce1(_ sender: Any) {
        
        player.play()
    }

    
    
    
    // Función que carga el audio seleccionado.
    func cargaAudio(nombre:String, player: inout AVAudioPlayer!)  {
        
        guard let sound = NSDataAsset(name: nombre) else {
            print("Audio no encontrado!!")
            return
        }
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            player = try AVAudioPlayer(data: sound.data)
        } catch let error as NSError {
            print("Error de reproducción: \(error.localizedDescription)")
        }
    }

   
    
    
    
} // Final bloque ViewController

